import argparse
import os

import config
from tg_lib import Telegram


def create_tg_object():
    tg = Telegram(
        api_id=config.api_id,
        api_hash=config.api_hash,
        phone=config.phone,  # you can pass 'bot_token' instead
        database_encryption_key='changekey123',
    )
    tg.login()
    result = tg.get_chats()
    result.wait()
    return tg


def pars_messages(result_folder, group_names, pars_count, only_text_msgs):
    """

    :param result_folder:
    :param group_names:
    :param pars_count: Количество максимального числа сообщений из каждой группы
    :param only_text_msgs:
    :return:
    """
    tg = create_tg_object()
    for group_name in group_names:
        with open(os.path.join(result_folder, f'{group_name}.txt'), 'w') as file:
            tg_obj: Telegram
            tg.pars_chat_history(group_name, file, pars_count, only_text_msgs=only_text_msgs)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--group_names_file", type=str)
    parser.add_argument("--count", type=int)
    parser.add_argument("--only_text_msgs", type=bool, default=False)
    args = parser.parse_args()
    with open(args.group_names_file) as file:
        group_names = file.read().split('\n')
    if args.only_text_msgs:
        only_text_msgs = True
    else:
        only_text_msgs = args.only_text_msgs
    pars_messages('results', group_names, args.count, only_text_msgs=only_text_msgs)
