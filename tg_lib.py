# import io
import json
import time
from pprint import pprint
from typing import io

from telegram.client import Telegram as OriginTelegram
from telegram.utils import AsyncResult


class Telegram(OriginTelegram):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def search_public_chat(self, username: str) -> AsyncResult:
        data = {
            '@type': 'searchPublicChat',
            'username': username,
        }

        return self._send_data(data)

    def set_proxy(self, proxy_line: str):
        # dont work
        proxy_line = proxy_line.split(':')
        data = {
            '@type': 'setProxy',
            'proxy': {'@type': 'proxySocks5',
                      'server': proxy_line[0],
                      'port': proxy_line[1],
                      'username': proxy_line[2],
                      'password': proxy_line[3]}
        }
        return self._send_data(data)

    def pars_chat_history(self, chat_name, wf: io.TextIO, pars_count: int, only_text_msgs=False, from_message_id=0):
        """

        :param from_message_id: с какого id сообщения начинать, если 0, то начнет с последнего
        :param chat_name: имя чата, если это ссылка(t.me/Sputnik_results), то  имя Sputnik_results
        :param wf: StringIO, для записи данных, будет вызываться wf.write()
        :param pars_count: количество сообщений для парсинга
        :param only_text_msgs: Парсить только текстовые сообщения(messageText), если False, то парсит все, в том числе
         фотографии
        :return:
        """
        print(chat_name)
        result = self.search_public_chat(username=chat_name)
        result.wait()
        chat_id = result.update['id']
        msgs_count = 0
        last_message_id = -1
        zero_last_msgs_count = 0
        while msgs_count < pars_count:
            r = self.get_chat_history(chat_id, limit=100, from_message_id=from_message_id)
            r.wait()
            count = len(r.update['messages'])
            if count == 0:
                pass
                zero_last_msgs_count += 1
                if zero_last_msgs_count > 20:
                    return True
                # print('r.update', r.update)
                # print('нет сообщений')
                # return True
            else:
                from_message_id = r.update['messages'][-1]['id']
                msgs = r.update['messages']
                if only_text_msgs:
                    msgs = [msg for msg in msgs if msg['content']['@type'] == 'messageText']
                texts = '\n'.join([json.dumps(msg, ensure_ascii=False) for msg in msgs])
                texts += '\n'
                msgs_count += len(msgs)
                if len(msgs) > 0:
                    wf.write(texts)
                else:
                    pass
                if last_message_id == r.update['messages'][-1]['id']:
                    time.sleep(30)
                # print(str(msgs))
            print('Count: ', msgs_count)

            time.sleep(0.3)


